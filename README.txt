Entity Content Visibility

This module has no UI interface

It only provides a field type and a field widget so that other modules can control the visibility of their own entities using the same contexts as Drupal blocks.

Only install this module if it is a dependency on other modules. For example the popup_entity module
