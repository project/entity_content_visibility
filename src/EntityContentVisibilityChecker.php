<?php

namespace Drupal\entityContent_visibility;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Plugin\Context\ContextHandler;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class EntityContentVisibilityChecker {

  /**
   * {@inheritdoc}
   */
  public static function createFromId(ContainerInterface $container, $entity_type, $id) {
    $entity_manager = $container->get('entity.manager');
    $entityContent = $entity_manager->getStorage($entity_type)->load($id);

    return new self(
      $entityContent,
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('context.handler')
    );
  }

  /**
   * The ConditionManager.
   *
   * @var \Drupal\Core\Condition\ConditionManager*/
  private $conditionManager;

  /**
   * The ContextRepositoryInterface.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  private $contextRepository;

  /**
   * The ContextHandler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandler*/
  private $contextHandler;

  /**
   * The $entityContent.
   *
   * @var entityContent
   */
  private $entityContent;

  /**
   * {@inheritdoc}
   */
  private function __construct($entityContent, ConditionManager $conditionManager, ContextRepositoryInterface $contextRepository, ContextHandler $contextHandler) {
    $this->entityContent = $entityContent;
    $this->conditionManager = $conditionManager;
    $this->contextRepository = $contextRepository;
    $this->contextHandler = $contextHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    $visibility = unserialize($this->entityContent->get('visibility')->value);
    if (!empty($visibility)) {
      foreach ($visibility as $condition_id => $condition_configuration) {
        if (!$this->evaluateCondition($condition_id, $condition_configuration)) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  private function evaluateCondition($condition_id, $condition_configuration) {
    /** @var \Drupal\Core\Condition\ConditionInterface $condition */
    $condition = $this->conditionManager->createInstance($condition_id, $condition_configuration);
    if ($condition instanceof ContextAwarePluginInterface) {
      $contexts = $this->contextRepository->getRuntimeContexts(array_values($condition->getContextMapping()));
      try {
        $this->contextHandler->applyContextMapping($condition, $contexts);
      }
      catch (ContextException $e) {
        return TRUE;
      }
    }

    return $condition->evaluate();
  }

}
