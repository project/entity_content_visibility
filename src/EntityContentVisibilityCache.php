<?php

namespace Drupal\entity_content_visibility;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Plugin\Context\ContextHandler;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class EntityContentVisibilityCache {

  /**
   * {@inheritdoc}
   */
  public static function createFromId(ContainerInterface $container, $entity_type, $id) {
    $entity_manager = $container->get('entity.manager');
    $entityContent = $entity_manager->getStorage($entity_type)->load($id);

    return new self(
      $entityContent,
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('context.handler')
    );
  }

  /**
   * The  ConditionManager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  private $conditionManager;

  /**
   * The ContextRepositoryInterface.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  private $contextRepository;

  /**
   * The ContextHandler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandler
   */
  private $contextHandler;

  /**
   * The entityContent.
   *
   * @var entityContent
   */
  private $entityContent;

  /**
   * The ConditionInterface.
   *
   * @var \Drupal\Core\Condition\ConditionInterface[]
   */
  private $conditions;

  /**
   * {@inheritdoc}
   */
  private function __construct($entityContent, ConditionManager $conditionManager, ContextRepositoryInterface $contextRepository, ContextHandler $contextHandler) {
    $this->entityContent = $entityContent;
    $this->conditionManager = $conditionManager;
    $this->contextRepository = $contextRepository;
    $this->contextHandler = $contextHandler;

    $this->conditions = $this->buildConditions();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cache_contexts = [];
    foreach ($this->conditions as $condition) {
      $cache_contexts = Cache::mergeContexts($cache_contexts, $condition->getCacheContexts());
    }
    return $cache_contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $cache_tags = [];
    foreach ($this->conditions as $condition) {
      $cache_tags = Cache::mergeTags($cache_tags, $condition->getCacheTags());
    }
    return $cache_tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $cache_max_age = Cache::PERMANENT;
    foreach ($this->conditions as $condition) {
      $cache_max_age = Cache::mergeMaxAges($cache_max_age, $condition->getCacheMaxAge());
    }
    return $cache_max_age;
  }

  /**
   * {@inheritdoc}
   */
  private function buildConditions() {
    $conditions = [];
    $visibility = unserialize($this->entityContent->get('visibility')->value);
    if ($visibility) {
      foreach ($visibility as $condition_id => $condition_configuration) {
        /** @var \Drupal\Core\Condition\ConditionInterface $condition */
        $condition = $this->conditionManager->createInstance($condition_id, $condition_configuration);
        if ($condition instanceof ContextAwarePluginInterface) {
          $contexts = $this->contextRepository->getRuntimeContexts(array_values($condition->getContextMapping()));
          try {
            $this->contextHandler->applyContextMapping($condition, $contexts);
          }
          catch (ContextException $e) {

          }
        }
        $conditions[$condition_id] = $condition;
      }
    }
    return $conditions;
  }

}
